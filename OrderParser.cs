﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace CodingExercise
{
    internal static class OrderParser
    {
        public static List<Order> Parse(string jsonFilename)
        {
            var jsonString = File.ReadAllText(jsonFilename);

            using var stringReader = new StringReader(jsonString);
            using var jsonTextReader = new JsonTextReader(stringReader);
            var jsonSerializer = new JsonSerializer();

            var jsonOrders = jsonSerializer.Deserialize<Dictionary<string, JsonOrder>>(jsonTextReader);

            return jsonOrders.Select(pair => new Order(ParseOrderId(pair.Key), pair.Value.Destination)).ToList();
        }

        private static string ParseOrderId(string order)
        {
            return order.ToLowerInvariant().Replace("order-", "");
        }

        private class JsonOrder
        {
            public string Destination { get; set; }
        }
    }
}