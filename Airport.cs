﻿namespace CodingExercise
{
    internal class Airport
    {
        public string IataAirportCode { get; }

        public string Name { get; }

        public Airport(string iataAirportCode, string name = "")
        {
            this.IataAirportCode = iataAirportCode;
            this.Name = name;
        }
    }
}