﻿using System;

namespace CodingExercise
{
    public static class Program
    {
        private static readonly FlightManager FlightManager = new FlightManager();

        public static void Main(string[] args)
        {
            int userInput;

            do
            {
                DisplayMenu();
                userInput = ReadUserInput();

                switch (userInput)
                {
                    case 1:
                        CreateFlight();
                        break;
                    case 2:
                        DisplayFlights();
                        break;
                    case 3:
                        ScheduleOrders();
                        break;
                    default:
                        Console.WriteLine("Invalid choice" + Environment.NewLine);
                        break;
                }
            } while (userInput != 4);
        }

        private static void DisplayMenu()
        {
            Console.WriteLine("1. Load a flight");
            Console.WriteLine("2. List flights");
            Console.WriteLine("3. Schedule orders");
            Console.WriteLine("4. Exit" + Environment.NewLine);
        }

        private static int ReadUserInput()
        {
            var userInput = Console.ReadLine();

            if (int.TryParse(userInput, out var choice))
            {
                return choice;
            }

            return 0;
        }

        private static void CreateFlight()
        {
            Console.WriteLine("Enter the departure IATA airport code");
            var departureAirport = new Airport(Console.ReadLine());

            Console.WriteLine("Enter the arrival IATA airport code");
            var arrivalAirport = new Airport(Console.ReadLine());

            // TODO: Could validate input here to avoid crash
            Console.WriteLine("Enter the day");
            var day = int.Parse(Console.ReadLine());

            FlightManager.AddFlight(departureAirport, arrivalAirport, day);

            Console.WriteLine("Flight added!" + Environment.NewLine);
        }

        private static void DisplayFlights()
        {
            Console.WriteLine(Environment.NewLine + "Flights:");

            foreach (var flight in FlightManager.Flights)
            {
                Console.WriteLine($"Flight: {flight.Id}, departure: {flight.DepartureAirport.IataAirportCode}, arrival: {flight.ArrivalAirport.IataAirportCode}, day: {flight.Day}");
            }
        }        
        
        private static void ScheduleOrders()
        {
            // TODO: Ask filename path here?
            var orders = OrderParser.Parse("coding-assigment-orders.json");

            foreach (var order in orders)
            {
                var orderFlight = FlightManager.ScheduleOrder(order);

                Console.WriteLine(orderFlight != null
                    ? $"order: order-{order.Id}, flightNumber: {orderFlight.Id}, departure: {orderFlight.DepartureAirport.IataAirportCode}, arrival: {orderFlight.ArrivalAirport.IataAirportCode}, day: {orderFlight.Day}"
                    : $"order: order-{order.Id}, flightNumber: not scheduled");
            }
        }

        private static void DisplayScheduledOrders()
        {
            Console.WriteLine(Environment.NewLine + "Orders:");

            foreach (var flight in FlightManager.Flights)
            {
                foreach (var order in flight.Orders)
                {
                    Console.WriteLine($"order: order-{order.Id}, flightNumber: {flight.Id}, departure: {flight.DepartureAirport.IataAirportCode}, arrival: {flight.ArrivalAirport.IataAirportCode}, day: {flight.Day}");
                }
            }
        }
    }
}
