﻿namespace CodingExercise
{
    internal class Order
    {
        public string Id { get; }

        public string Destination { get; }

        public Order(string id, string destination)
        {
            this.Id = id;
            this.Destination = destination;
        }
    }
}