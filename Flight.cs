﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CodingExercise
{
    internal class Flight
    {
        // Hardcode the box capacity here for now
        private const int BoxCapacity = 20;

        private readonly List<Order> _orders;

        public int Id { get; }

        public Airport DepartureAirport { get; }

        public Airport ArrivalAirport { get; }

        public int Day { get; }

        public IReadOnlyCollection<Order> Orders => new ReadOnlyCollection<Order>(this._orders);

        public Flight(int id, Airport departureAirport, Airport arrivalAirport, int day)
        {
            this.Id = id;
            this.DepartureAirport = departureAirport;
            this.ArrivalAirport = arrivalAirport;
            this.Day = day;
            this._orders = new List<Order>(BoxCapacity);
        }

        public bool IsFull()
        {
            return this.Orders.Count == BoxCapacity;
        }

        public bool ScheduleOrder(Order order)
        {
            if (this.IsFull())
            {
                return false;
            }
            
            this._orders.Add(order);

            return true;
        }
    }
}