﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CodingExercise
{
    internal class FlightManager
    {
        private readonly List<Flight> _flights;

        public IReadOnlyCollection<Flight> Flights => new ReadOnlyCollection<Flight>(this._flights);

        public FlightManager()
        {
            this._flights = new List<Flight>();
        }

        public Flight AddFlight(Airport departureAirport, Airport arrivalAirport, int day)
        {
            var flight = new Flight(this.Flights.Count + 1, departureAirport, arrivalAirport, day);
            this._flights.Add(flight);

            return flight;
        }

        public Flight ScheduleOrder(Order order)
        {
            var nextAvailableFlight = this.GetNextAvailableFlight(order.Destination);

            if (nextAvailableFlight != null)
            {
                var isOrderScheduled = nextAvailableFlight.ScheduleOrder(order);

                if (isOrderScheduled)
                    return nextAvailableFlight;
            }

            return null;
        }

        private Flight GetNextAvailableFlight(string destination)
        {
            return this.Flights.FirstOrDefault(x => !x.IsFull() && destination.Equals(x.ArrivalAirport.IataAirportCode));
        }
    }
}